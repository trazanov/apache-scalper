//+------------------------------------------------------------------+
//|                                                      Signals.mqh |
//|                                                       a.trazanov |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "a.trazanov"
#property link      "https://www.mql5.com"

#include "TradeLib.mqh"


struct order{
   ENUM_ORDER_TYPE type;
   string pair;
   
   string toString()
   {
      return pair + " | " + EnumToString(type) + " *** ";
   }
};

struct strategy_stoch
{
   double sell_level;
   double buy_level;
   double refresh_level;
   
   ENUM_ORDER_TYPE Signal(string symb)
   {
      int stoch = iStochastic(symb, PERIOD_CURRENT, 5, 3, 3, MODE_SMA, STO_CLOSECLOSE);
      double arr[];
      CopyBuffer(stoch, 0, 0, 5, arr);
      ArraySetAsSeries(arr, true);

      if(arr[0] <=80 && arr[1] >80)
      {
         return ORDER_TYPE_SELL;
      }
      
      if(arr[0] >=20 && arr[1] <20)
      {
         return ORDER_TYPE_BUY;
      }
      
      return -1;
   }
};



struct strategy_waterfall
  {
   double               count_of_bars;//Колличество свечей для анализа
   double               distance;//Колличество пунктов для сигнала
   double               deviation_bars_percent;//Допустимое колличество отклонений (в процентах)


   ENUM_ORDER_TYPE Signal(string symb, ENUM_TIMEFRAMES tf, int from_bar=1)
     {
      double res = deviation_bars_percent * (count_of_bars/100);
      double deviation_bars_count = MathRound(res);
      int count_for_buy=0;
      double dist_buy=0;
      int count_for_sell=0;
      double dist_sell=0;

      for(int i=from_bar;  i<=count_of_bars; i++)
        {
         mybar bar_x = GetBarType(symb, tf, i);
            
            if(bar_x.type==BEAR)
            {
               count_for_sell++;
               dist_sell = dist_sell + bar_x.distance;
            }
            
            if(bar_x.type==BULL)
            {
               count_for_buy++;
               dist_buy = dist_buy + bar_x.distance;
            } 
             
            if(bar_x.type==NEUTRALL)
            {
               count_for_buy++;
               count_for_sell++;
            } 
            
        }
        

        
        if(count_for_buy >= count_of_bars-deviation_bars_count && dist_buy >= distance)
        {
            return ORDER_TYPE_BUY;
        }
            
        if(count_for_sell >= count_of_bars-deviation_bars_count && dist_sell >= distance)
        {
            return ORDER_TYPE_SELL;
        }
      
         
      return -1;
     }
  };
//+------------------------------------------------------------------+
