//+------------------------------------------------------------------+
//|                                                     TradeLib.mqh |
//|                                                       a.trazanov |
//|                                                                  |
//+------------------------------------------------------------------+
#property copyright "a.trazanov"
#property link      ""
input double AUTOLOT = 0.0001;
#include <Trade\Trade.mqh>
CTrade trade;


enum ENUM_TREND_TYPE
  {
   BULL=0,
   BEAR=1,
   NEUTRALL=-1
  };

struct mybar
  {
   ENUM_TREND_TYPE     type;
   double            distance;

   string            toString()
     {
      return IntegerToString(type) + "___" + DoubleToString(distance,2);
     }
  };



//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
bool OpenOrder(ENUM_ORDER_TYPE type, string symbol, double tp, double sl, MqlTradeResult &res, bool real_tp=false, bool real_sl=false, double lts = 0)
  {
   MqlTradeRequest req = {};

   double ask = SymbolInfoDouble(symbol, SYMBOL_ASK);
   double bid = SymbolInfoDouble(symbol, SYMBOL_BID);
   double point = SymbolInfoDouble(symbol, SYMBOL_POINT);
   long spread = SymbolInfoInteger(symbol, SYMBOL_SPREAD);

   req.type = type;
   req.action = TRADE_ACTION_DEAL;
   req.symbol = symbol;
   double lot = NormalizeDouble(AccountInfoDouble(ACCOUNT_BALANCE) * AUTOLOT,2);
   req.volume   = lot<0.01 ? 0.01 : lot;
   lts == 0 ? true : req.volume = lts;
   req.type_filling=ORDER_FILLING_FOK;
   req.deviation = 90;
   req.price = type==ORDER_TYPE_BUY ? ask : bid;
   type==ORDER_TYPE_BUY ? req.sl = ask - sl *point : req.sl = bid + sl *point;
   type==ORDER_TYPE_BUY ? req.tp = ask + tp *point : req.tp = bid - tp *point;
   real_tp ? req.tp = tp : true;
   real_sl ? req.sl = sl : true;
   sl == 0 ? req.sl = 0 : true;
   tp == 0 ? req.tp = 0 : true;
   return OrderSend(req,res);
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
mybar GetBarType(string symb, ENUM_TIMEFRAMES period, int pos)
  {
   mybar bar;
   double point = SymbolInfoDouble(symb,SYMBOL_POINT);
   double close = iClose(symb, period, pos);
   double open = iOpen(symb, period, pos);

   if(close<open)
     {
      bar.type=BEAR;
      bar.distance=(open-close)/point;

      return bar;
     }

   if(close>open)
     {
      bar.type=BULL;
      bar.distance=(close-open)/point;

      return bar;
     }

   bar.type=NEUTRALL;
   bar.distance=0;
   return bar;
  }
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
ENUM_ORDER_TYPE ZigZagTrend(string symb)
  {
   int zz_h = iCustom(NULL, PERIOD_CURRENT, "ZigZag", 12, 5, 3);
   double arr[];
   CopyBuffer(zz_h, 0, 0, 200, arr);
   ArraySetAsSeries(arr, true);
   double first_value = -1;
   for(int i=0; i<ArraySize(arr); i++)
     {
      if(arr[i]==0)
         continue;

      if(first_value==-1)
         first_value = arr[i];

      if(first_value < arr[i])
         return ORDER_TYPE_SELL;

      if(first_value > arr[i])
         return ORDER_TYPE_BUY;

     }

   return -1;
  }
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
double ZigZagVal(string symb)
  {
   int zz_h = iCustom(NULL, PERIOD_CURRENT, "ZigZag", 12, 5, 3);
   double arr[];
   CopyBuffer(zz_h, 0, 0, 1, arr);
   ArraySetAsSeries(arr, true);

   return arr[0];
  }
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void SimpleTrall(double step, double dist, double start)
  {

   int total = PositionsTotal();
   for(int i = 0; i <= total; i++)
     {
      if(!PositionSelect(PositionGetSymbol(i)))
         continue;

      string symb = PositionGetString(POSITION_SYMBOL);

      double ask = SymbolInfoDouble(symb, SYMBOL_ASK);
      double bid = SymbolInfoDouble(symb, SYMBOL_BID);
      double point = SymbolInfoDouble(symb, SYMBOL_POINT);
      long spread = SymbolInfoInteger(symb, SYMBOL_SPREAD);

      bool type = PositionGetInteger(POSITION_TYPE);
      double sl = PositionGetDouble(POSITION_SL);
      double tp = PositionGetDouble(POSITION_TP);
      double price_cur = PositionGetDouble(POSITION_PRICE_CURRENT);
      double price_open = PositionGetDouble(POSITION_PRICE_OPEN);
      long ticket = PositionGetInteger(POSITION_TICKET);

      if(type==ORDER_TYPE_BUY)
        {
         if(price_cur - price_open >= start*point)
           {
            double new_sl = price_cur - dist*point;

            if(new_sl > sl)
               PositionModify(symb, ticket, new_sl, tp);
           }
        }

      if(type==ORDER_TYPE_SELL)
        {
         if(price_open - price_cur >= start*point)
           {
            double new_sl = price_cur + dist*point;

            if(new_sl < sl)
               PositionModify(symb, ticket, new_sl, tp);
           }
        }
     }



  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void PositionModify(string symbol, long ticket, double new_sl, double new_tp)
  {
   MqlTradeRequest req = {};
   MqlTradeResult res = {};

   req.action = TRADE_ACTION_SLTP;
   req.symbol = symbol;
   req.position   = ticket;
   req.sl = new_sl;
   req.tp = new_tp;

   if(!OrderSend(req,res))
      PrintFormat("OrderSend error %d",GetLastError());  // если отправить запрос не удалось, вывести код ошибки

   PrintFormat("retcode=%u  deal=%I64u  order=%I64u",res.retcode,res.deal,res.order);
  }
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void MonitorUnlose(double distance, string symbol=NULL, bool order_type=0)
  {
   ulong ticket = GetLastTicket(symbol, order_type);

   if(PositionSelectByTicket(ticket))
     {
      string symb = PositionGetString(POSITION_SYMBOL);
      double point = SymbolInfoDouble(symb, SYMBOL_POINT);


      ENUM_ORDER_TYPE type = (ENUM_ORDER_TYPE)PositionGetInteger(POSITION_TYPE);
      double sl = PositionGetDouble(POSITION_SL);
      double tp = PositionGetDouble(POSITION_TP);
      double price_cur = PositionGetDouble(POSITION_PRICE_CURRENT);
      double price_open = PositionGetDouble(POSITION_PRICE_OPEN);

      if(type==ORDER_TYPE_BUY)
        {
         if(price_open - price_cur >= distance*point)
           {
            MqlTradeResult res;
            if(OpenOrder(type, symb, PositionGetDouble(POSITION_TP), 0, res, true, true, PositionGetDouble(POSITION_VOLUME)))
               return;
           }
        }

      if(type==ORDER_TYPE_SELL)
        {
         if(price_cur - price_open >= distance*point)
           {
            MqlTradeResult res;
            if(OpenOrder(type, symb, PositionGetDouble(POSITION_TP), 0, res, true, true, PositionGetDouble(POSITION_VOLUME)))
               return;
           }
        }
     }
  }
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
ulong GetLastTicket(string symbol, bool type)
  {
   int total = PositionsTotal();
   for(int i = total-1 ; i >= 0 ; i--)
     {
      if(!PositionSelectByTicket(PositionGetTicket(i)))
         continue;

      if(symbol == PositionGetString(POSITION_SYMBOL) && type == PositionGetInteger(POSITION_TYPE))
         return PositionGetInteger(POSITION_TICKET);
     }

   return -1;
  }
struct deals
  {
   double            profit;
   int               count;
  };

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
deals SummaryProfitInPoints(string symbol, bool type)
  {
   deals deal = {0,0};
   int total = PositionsTotal();
   for(int i = 0 ; i <= total ; i++)
     {
      if(!PositionSelectByTicket(PositionGetTicket(i)))
         continue;

      if(symbol == PositionGetString(POSITION_SYMBOL) && type == PositionGetInteger(POSITION_TYPE))
        {
         double price_cur = PositionGetDouble(POSITION_PRICE_CURRENT);
         double price_opn = PositionGetDouble(POSITION_PRICE_OPEN);
         double point = SymbolInfoDouble(symbol, SYMBOL_POINT);

         type ? deal.profit += (price_opn - price_cur)/point : deal.profit += (price_cur - price_opn)/point;
         deal.count++;
        }
     }
   return deal;
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void CloseLogic(string symbol, double close_points, double take, bool type)
  {
   deals d = SummaryProfitInPoints(symbol, type);
   double final_profit = take + (close_points * d.count);
   if(d.count>0)
      Comment("Надо: " + DoubleToString(final_profit) + "\nСейчас прибыль: " + DoubleToString(d.profit,1) + "\nВсего сделок: " + IntegerToString(d.count) );
      
   if(d.profit >= final_profit)
      PositionsClose(symbol);
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void PositionsClose(string symbol)
  {
   while(PositionsTotal()>0)
   {
     while(trade.PositionClose(symbol) )
     {
      Print("Close");
     }
     Sleep(1000);
     
     if(PositionsTotal()>0)
         Print("Что-то не задалось! Доведём до конца!");
   }
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void depricated_PositionClose(ulong ticket)
  {
   MqlTradeRequest req = {};
   MqlTradeResult res = {};
   if(PositionSelectByTicket(ticket))
     {
     bool pos_type = PositionGetInteger(POSITION_TYPE);
     string symb = PositionGetString(POSITION_SYMBOL);
     
      req.position = ticket;
      req.action = TRADE_ACTION_DEAL;
      req.volume = PositionGetDouble(POSITION_VOLUME);
      pos_type ? req.type = ORDER_TYPE_BUY : req.type = ORDER_TYPE_SELL;
      pos_type ? req.price = SymbolInfoDouble(symb, SYMBOL_BID) : req.price = SymbolInfoDouble(symb, SYMBOL_ASK);
      
      if(!OrderSend(req,res))
         Print(res.retcode);
     }
  }
//+------------------------------------------------------------------+
