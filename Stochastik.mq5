//+------------------------------------------------------------------+
//|                                                   Stochastik.mq5 |
//|                                  Copyright 2021, MetaQuotes Ltd. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2021, MetaQuotes Ltd."
#property link      "https://www.mql5.com"
#property version   "1.00"

#include "TradeLib.mqh"
#include "Signals.mqh"
//+------------------------------------------------------------------+
//| Expert initialization function                                   |
//+------------------------------------------------------------------+
int OnInit()
  {
//---
      string s = "EURUSD,AUDUSD,GBPUSD,NZDUSD,USDJPY,XAUUSD,USDCHF,GBPJPY,GBPCHF,GBPNZD,GBPAUD,USDCAD,GBPCAD,AUDCAD,NZDCAD";
      string allow_symb[];
      StringSplit(s,',',allow_symb);
      for(int j=0; j<8; j++)
      {
         SymbolSelect(allow_symb[j],true);
         
      }
//---
   return(INIT_SUCCEEDED);
  }
//+------------------------------------------------------------------+
//| Expert deinitialization function                                 |
//+------------------------------------------------------------------+
void OnDeinit(const int reason)
  {
//---
   
  }
//+------------------------------------------------------------------+
//| Expert tick function                                             |
//+------------------------------------------------------------------+
void OnTick()
  {
//---
   order signals[];
   ArrayResize(signals,1);

   
   strategy_stoch st;
   int total = SymbolsTotal(true);
   int q = 0;
   string x = "";
   for(int i = 0; i<total; i++)
   {
      string symbol = SymbolName(i, true);
      
      if(true)
      {
         ENUM_ORDER_TYPE type = st.Signal(symbol);
         
         if(type != -1)
         {
            ArrayResize(signals, q+2, 1000);
            signals[q].pair = symbol;
            signals[q].type = type;
            
            x = x+signals[q].toString();
            q++;
         } 
      }
   }
   if(x!="")   Alert(x);
  }
//+------------------------------------------------------------------+
