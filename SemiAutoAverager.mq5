//+------------------------------------------------------------------+
//|                                             SemiAutoAverager.mq5 |
//|                                                       a.trazanov |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "a.trazanov"
#property link      "https://www.mql5.com"
#property version   "1.00"
input int Distance=100;//Усреднения каждые
input int Points_for_close = 100;//Закрытие при достижении этого * на кол-во сделок


struct cur
  {
   string            symb;
   double            total_profit;
   int               count;
   bool              type;

   string            toString()
     {
      return "Symbol: " + symb + " | Count: " + IntegerToString(count) + " | Profit: " + DoubleToString(total_profit,2) + "| Type: " + BoolToOrderType(type) + " | NeedToClose: " + DoubleToString(count*Points_for_close,1) + "\n";
     }
  };

struct deals
  {
   double            profit;
   int               count;
  };

//+------------------------------------------------------------------+
//| Expert initialization function                                   |
//+------------------------------------------------------------------+
int OnInit()
  {
   LabelCreate(0, "MY_Dist",0, 5, 70, CORNER_LEFT_LOWER, "Distance: " + IntegerToString(Distance), "Arial", 20, clrAqua);
   LabelCreate(0, "MY_Points",0, 5, 40, CORNER_LEFT_LOWER, "Points for close: " + IntegerToString(Points_for_close), "Arial", 20, clrAliceBlue);
   
   return(INIT_SUCCEEDED);
  }
//+------------------------------------------------------------------+
//| Expert deinitialization function                                 |
//+------------------------------------------------------------------+
void OnDeinit(const int reason)
  {
//---
   ObjectsDeleteAll(0,0,OBJ_LABEL);

  }
//+------------------------------------------------------------------+
//| Expert tick function                                             |
//+------------------------------------------------------------------+
void OnTick()
  {
   Panel();

  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void Panel()
  {
   cur curs[];
   cur c = {0};

   int total = PositionsTotal();
   for(int i = 0; i<=total; i++)
     {
      if(!PositionSelectByTicket(PositionGetTicket(i)))
         continue;

      string sym = PositionGetString(POSITION_SYMBOL);
      bool typ = PositionGetInteger(POSITION_TYPE);
      c.total_profit = SummaryProfitInPoints(sym, typ).profit;
      c.type = typ;
      c.symb = sym;

      PushCur(c, curs);
     }
   string summ = "";
   for(int i = 0; i < ArraySize(curs); i++)
     {
      summ += curs[i].toString();
      if(Points_for_close*curs[i].count <= curs[i].total_profit)
      {
         PositionsClose(curs[i].symb, curs[i].type);
         Print("Закрыли " + curs[i].symb);
      }
     }
   Comment(summ);
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
int PushCur(cur &element, cur &curs[])
  {
   int size = ArraySize(curs);
   for(int i = 0; i < size; i++)
     {
      if(element.symb == curs[i].symb && element.type == curs[i].type)
        {
         curs[i].count++;
         curs[i].total_profit = element.total_profit;
         return i;
        }
     }
   ArrayResize(curs, size+1);
   element.count = 1;
   curs[size] = element;
   curs[size].type = element.type;
   curs[size].symb = element.symb;

   return 0;
  }
//+------------------------------------------------------------------+
//| ChartEvent function                                              |
//+------------------------------------------------------------------+
void OnChartEvent(const int id,
                  const long &lparam,
                  const double &dparam,
                  const string &sparam)
  {
//---

  }
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
ulong GetLastTicket(string symbol, bool type)
  {
   int total = PositionsTotal();
   for(int i = total-1 ; i >= 0 ; i--)
     {
      if(!PositionSelectByTicket(PositionGetTicket(i)))
         continue;

      if(symbol == PositionGetString(POSITION_SYMBOL) && type == PositionGetInteger(POSITION_TYPE))
         return PositionGetInteger(POSITION_TICKET);
     }

   return -1;
  }



//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
deals SummaryProfitInPoints(string symbol, bool type)
  {
   deals deal = {0,0};
   int total = PositionsTotal();
   for(int i = 0 ; i <= total ; i++)
     {
      if(!PositionSelectByTicket(PositionGetTicket(i)))
         continue;

      if(symbol == PositionGetString(POSITION_SYMBOL) && type == PositionGetInteger(POSITION_TYPE))
        {
         double price_cur = PositionGetDouble(POSITION_PRICE_CURRENT);
         double price_opn = PositionGetDouble(POSITION_PRICE_OPEN);
         double point = SymbolInfoDouble(symbol, SYMBOL_POINT);

         type ? deal.profit += (price_opn - price_cur)/point : deal.profit += (price_cur - price_opn)/point;
         deal.count++;
        }
     }
   return deal;
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void CloseLogic(string symbol, double close_points, double take, bool type)
  {
   deals d = SummaryProfitInPoints(symbol, type);
   double final_profit = take + (close_points * d.count);
//  if(d.count>0)
//     Comment("Надо: " + DoubleToString(final_profit,2) + "\nСейчас прибыль: " + DoubleToString(d.profit, 0) + "\nВсего сделок: " + IntegerToString(d.count, 3));
   if(d.profit >= final_profit)
      PositionsClose(symbol, type);
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void PositionsClose(string symbol, bool type)
  {
      int total = PositionsTotal();
      for(int i = 0; i<=total; i++)
        {
         if(!PositionSelectByTicket(PositionGetTicket(i)))
            continue;

         if(symbol == PositionGetString(POSITION_SYMBOL) && type == PositionGetInteger(POSITION_TYPE))
           {
            PositionClose(PositionGetInteger(POSITION_TICKET));
           }
        }
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void PositionClose(ulong ticket)
  {
   MqlTradeRequest req = {0};
   MqlTradeResult res = {0};
   if(PositionSelectByTicket(ticket))
     {
      bool pos_type = PositionGetInteger(POSITION_TYPE);
      string symb = PositionGetString(POSITION_SYMBOL);

      req.position = ticket;
      req.action = TRADE_ACTION_DEAL;
      req.volume = PositionGetDouble(POSITION_VOLUME);
      pos_type ? req.type = ORDER_TYPE_BUY : req.type = ORDER_TYPE_SELL;
      pos_type ? req.price = SymbolInfoDouble(symb, SYMBOL_BID) : req.price = SymbolInfoDouble(symb, SYMBOL_ASK);

      if(!OrderSend(req,res))
         Print(res.retcode);
     }
  }
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
void MonitorUnlose(double distance, string symbol=NULL, bool order_type=0)
  {
   ulong ticket = GetLastTicket(symbol, order_type);

   if(PositionSelectByTicket(ticket))
     {
      string symb = PositionGetString(POSITION_SYMBOL);
      double point = SymbolInfoDouble(symb, SYMBOL_POINT);


      ENUM_ORDER_TYPE type = (ENUM_ORDER_TYPE)PositionGetInteger(POSITION_TYPE);
      double sl = PositionGetDouble(POSITION_SL);
      double tp = PositionGetDouble(POSITION_TP);
      double price_cur = PositionGetDouble(POSITION_PRICE_CURRENT);
      double price_open = PositionGetDouble(POSITION_PRICE_OPEN);

      if(type==ORDER_TYPE_BUY)
        {
         if(price_open - price_cur >= distance*point)
           {
            MqlTradeResult res;
            if(OpenOrder(type, symb, 0, 0, res, 100, 5, 1, PositionGetDouble(POSITION_VOLUME)))
               return;
           }
        }

      if(type==ORDER_TYPE_SELL)
        {
         if(price_cur - price_open >= distance*point)
           {
            MqlTradeResult res;
            if(OpenOrder(type, symb, 0, 0, res, 100, 5, 1, PositionGetDouble(POSITION_VOLUME)))
               return;
           }
        }
     }
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
bool OpenOrder(ENUM_ORDER_TYPE type, string symbol, double tp, double sl, MqlTradeResult &res, int maxspread, int tryOpen=5, int sleepAfterTry=1, double lts = 0)
  {
   MqlTradeRequest req = {0};;

   double ask = SymbolInfoDouble(symbol, SYMBOL_ASK);
   double bid = SymbolInfoDouble(symbol, SYMBOL_BID);
   double point = SymbolInfoDouble(symbol, SYMBOL_POINT);
   long spread = SymbolInfoInteger(symbol, SYMBOL_SPREAD);

   req.type = type;
   req.action = TRADE_ACTION_DEAL;
   req.symbol = symbol;
   double lot = 0.01;
   req.volume   = lot<0.01 ? 0.01 : lot;
   lts == 0 ? true : req.volume = lts;
   req.type_filling=ORDER_FILLING_FOK;
   req.deviation = 90;
   req.price = type==ORDER_TYPE_BUY ? ask : bid;
   type==ORDER_TYPE_BUY ? req.sl = ask - sl *point : req.sl = bid + sl *point;
   sl==0 ? req.sl = sl : 1;
   type==ORDER_TYPE_BUY ? req.tp = ask + tp *point : req.tp = bid - tp *point;
   tp==0 ? req.tp = tp : 1;
   return OrderSend(req,res);
  }
//+------------------------------------------------------------------+

string BoolToOrderType(bool type)
{
   if(type)
      return "SELL";
      
   return "BUY";
}

bool LabelCreate(const long              chart_ID=0,               // ID графика 
                 const string            name="Label",             // имя метки 
                 const int               sub_window=0,             // номер подокна 
                 const int               x=0,                      // координата по оси X 
                 const int               y=0,                      // координата по оси Y 
                 const ENUM_BASE_CORNER  corner=CORNER_LEFT_UPPER, // угол графика для привязки 
                 const string            text="Label",             // текст 
                 const string            font="Arial",             // шрифт 
                 const int               font_size=10,             // размер шрифта 
                 const color             clr=clrRed,               // цвет 
                 const double            angle=0.0,                // наклон текста 
                 const ENUM_ANCHOR_POINT anchor=ANCHOR_LEFT_UPPER, // способ привязки 
                 const bool              back=false,               // на заднем плане 
                 const bool              selection=false,          // выделить для перемещений 
                 const bool              hidden=true,              // скрыт в списке объектов 
                 const long              z_order=0)                // приоритет на нажатие мышью 
  { 
//--- сбросим значение ошибки 
   ResetLastError(); 
//--- создадим текстовую метку 
   if(!ObjectCreate(chart_ID,name,OBJ_LABEL,sub_window,0,0)) 
     { 
      Print(__FUNCTION__, 
            ": не удалось создать текстовую метку! Код ошибки = ",GetLastError()); 
      return(false); 
     } 
//--- установим координаты метки 
   ObjectSetInteger(chart_ID,name,OBJPROP_XDISTANCE,x); 
   ObjectSetInteger(chart_ID,name,OBJPROP_YDISTANCE,y); 
//--- установим угол графика, относительно которого будут определяться координаты точки 
   ObjectSetInteger(chart_ID,name,OBJPROP_CORNER,corner); 
//--- установим текст 
   ObjectSetString(chart_ID,name,OBJPROP_TEXT,text); 
//--- установим шрифт текста 
   ObjectSetString(chart_ID,name,OBJPROP_FONT,font); 
//--- установим размер шрифта 
   ObjectSetInteger(chart_ID,name,OBJPROP_FONTSIZE,font_size); 
//--- установим угол наклона текста 
   ObjectSetDouble(chart_ID,name,OBJPROP_ANGLE,angle); 
//--- установим способ привязки 
   ObjectSetInteger(chart_ID,name,OBJPROP_ANCHOR,anchor); 
//--- установим цвет 
   ObjectSetInteger(chart_ID,name,OBJPROP_COLOR,clr); 
//--- отобразим на переднем (false) или заднем (true) плане 
   ObjectSetInteger(chart_ID,name,OBJPROP_BACK,back); 
//--- включим (true) или отключим (false) режим перемещения метки мышью 
   ObjectSetInteger(chart_ID,name,OBJPROP_SELECTABLE,selection); 
   ObjectSetInteger(chart_ID,name,OBJPROP_SELECTED,selection); 
//--- скроем (true) или отобразим (false) имя графического объекта в списке объектов 
   ObjectSetInteger(chart_ID,name,OBJPROP_HIDDEN,hidden); 
//--- установим приоритет на получение события нажатия мыши на графике 
   ObjectSetInteger(chart_ID,name,OBJPROP_ZORDER,z_order); 
//--- успешное выполнение 
   return(true); 
  } 
  
  SymbolInfoDouble()