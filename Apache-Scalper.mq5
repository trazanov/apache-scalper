//+------------------------------------------------------------------+
//|                                               Apache-Scalper.mq5 |
//|                        Copyright 2021, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2021, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property version   "1.00"

input double DISTANCE_UNLOSS=500;//Колличество пунктов для БУ
input double POINTS_FOR_CLOSE=950;//Колличество пунктов для закрытия * на кол-вл сделок
double TRALL_DISTANCE = 300;
double TRALL_START = 30000;

input int COUNT=6;//Колличество баров из которых состоит промежуток
input int DEVIATION_PERCENT=20;//Колличество баров (в процентах) в промежутке, которое отличается от основного направления
input double DISTANCE=100;//Колличество пунктов за этот промежуток

input int TP=150;
input int SL=0;

#include "TradeLib.mqh"
#include "Signals.mqh"
//+------------------------------------------------------------------+
//| Expert initialization function                                   |
//+------------------------------------------------------------------+
int OnInit()
  {

   return(INIT_SUCCEEDED);
  }
//+------------------------------------------------------------------+
//| Expert deinitialization function                                 |
//+------------------------------------------------------------------+
void OnDeinit(const int reason)
  {

  }

//+------------------------------------------------------------------+
//| Expert tick function                                             |
//+------------------------------------------------------------------+
double first_zigzag=-1;
void OnTick()
  {
   MonitorUnlose(DISTANCE_UNLOSS, Symbol(), 0);
   MonitorUnlose(DISTANCE_UNLOSS, Symbol(), 1);
   CloseLogic(Symbol(), POINTS_FOR_CLOSE, TP, 0);
   CloseLogic(Symbol(), POINTS_FOR_CLOSE, TP, 1);
//SimpleTrall(0, TRALL_DISTANCE, TRALL_START);

   if(OrdersTotal()>0 || PositionsTotal()>0)
      return;

   strategy_waterfall strata;
   strata.count_of_bars=COUNT;
   strata.deviation_bars_percent=DEVIATION_PERCENT;
   strata.distance=DISTANCE;

   ENUM_ORDER_TYPE type = strata.Signal(Symbol(), PERIOD_CURRENT);

   if(type>-1)
     {
      MqlTradeResult res;

      OpenOrder(type, Symbol(), TP, SL, res);
     }

  }
//+------------------------------------------------------------------+
//| Tester function                                                  |
//+------------------------------------------------------------------+

string zz_type;
double OnTester()
  {
//---
   double ret=0.0;
//---

//---
   return(ret);
  }
//+------------------------------------------------------------------+
//| TesterInit function                                              |
//+------------------------------------------------------------------+
void OnTesterInit()
  {
//---

  }
//+------------------------------------------------------------------+
//| TesterPass function                                              |
//+------------------------------------------------------------------+
void OnTesterPass()
  {
//---

  }
//+------------------------------------------------------------------+
//| TesterDeinit function                                            |
//+------------------------------------------------------------------+
void OnTesterDeinit()
  {
//---

  }
//+------------------------------------------------------------------+
